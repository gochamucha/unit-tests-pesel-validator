package agh.tests;

import agh.qa.Pesel;

import java.text.ParseException;

public class PeselTests {

    @DataProvider
    public Object[][] testGetDigitDataProvider() {
        return new Object[][]{

                {"91093001128", -1, -1},
                {"91093001128", 0, 8},
                {"91093001128", 11, -1},
                {"91093001128", 10, 2},
        };
    }

    @Test(dataProvider = "testGetDigitDataProvider")
    public void testGetDigit(String toParse, int position, int expResult) throws ParseException {
        Pesel getDigitTester = PeselParser.Parse(toParse);
        int actIndex = getDigitTester.getDigit(position);
        Assert.assertEquals(actIndex, expResult);
    }
}
