package agh.tests;

import agh.qa.PeselValidator;
import java.text.ParseException;
import java.time.LocalDate;

public class PeselExtractorTests {

    @DataProvider
    public Object[][] peselExtractorSexTestDataProvider() {
        return new Object[][]{
                {"78011536214", "Male"},
                {"91093001128", "Female"},
        };
    }

    @DataProvider
    public Object[][] peselExtractorBirthDayTestDataProvider() {
        return new Object[][]{
                {"78011536214", "1978-01-15"},
                {"91093001128", "1991-09-30"},
        };
    }

    @Test(dataProvider = "peselExtractorSexTestDataProvider")
    public void testGetSex(String peselData, String expectedSex) throws ParseException {
        Pesel pesel = PeselParser.Parse(peselData);
        PeselExtractor peselExtractor = new PeselExtractor(pesel);

        Assert.assertEquals(peselExtractor.GetSex(), expectedSex);
    }

    @Test(dataProvider = "peselExtractorBirthDayTestDataProvider")
    public void testGetData(String peselData, String expectedBirthDate) throws ParseException {

        Pesel pesel = PeselParser.Parse(peselData);
        PeselExtractor peselExtractor = new PeselExtractor(pesel);

        Assert.assertEquals(peselExtractor.GetBirthDate(), LocalDate.parse(expectedBirthDate));
    }

}