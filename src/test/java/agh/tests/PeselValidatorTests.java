package agh.tests;

public class PeselValidatorTests {
    @DataProvider
    public Object[][] peselTestDataProvider() {
        return new Object[][]{
                        {"44051401359", true},
                        {"00301752851", true},
                        {"83053401352", false},
                        {"8305340135a", false},
                        {"830534013522", false},
                        {"20640744715", true},
                        {"83051601352", true},
                        {"44051401358", false},
                        {"8305160132", false},
                };
    }

    @Test(dataProvider = "peselTestDataProvider")
    public void TestPesel(String pesel, boolean shouldBeValid){
        PeselValidator validator = new PeselValidator();

        Assert.assertEquals(shouldBeValid, validator.validate(pesel));
    }
}
